#include "realTime.cpp"

using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;

//convenient typedefs
typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;
typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;

// Define a new point representation for < x, y, z, curvature >
class MyPointRepresentation : public pcl::PointRepresentation <PointNormalT>
{
  using pcl::PointRepresentation<PointNormalT>::nr_dimensions_;
public:
  MyPointRepresentation ()
  {
    // Define the number of dimensions
    nr_dimensions_ = 4;
  }

  // Override the copyToFloatArray method to define our feature vector
  virtual void copyToFloatArray (const PointNormalT &p, float * out) const
  {
    // < x, y, z, curvature >
    out[0] = p.x;
    out[1] = p.y;
    out[2] = p.z;
    out[3] = p.curvature;
  }
};

class Reconstruction{

	private:
		// visualizador
		boost::shared_ptr<pcl::visualization::PCLVisualizer> 	viewerUnion;
		boost::shared_ptr<pcl::visualization::PCLVisualizer> 	viewerReconstruction;
		Eigen::Matrix4f GlobalTransform, pairTransform;

		PointCloud::Ptr result;
		PointCloud::Ptr source;
		PointCloud::Ptr target;
		PointCloud::Ptr reconstruction;

	  	string pathToLoad;
	  	string name;
	  	string url;
	  	string pathReconstruct;
	  	int snapCount, numFiles, jumps, normalKserach, error, IterationsFor, vp_1, vp_2;
	  	float voxGrid,transforEpsilon,MaxIterationsAlig,rSimilarityThreshold,rMaxCorrespondenceDistance,rInlierFraction,MaxCorrDisSubs;
	  	bool vox,Bfloor;

	  	void saveConfig(){

	  		string saveTo = "./"+pathToLoad+"/"+pathReconstruct+"/Configuration.txt";
			std::ofstream outfile (saveTo.c_str(), std::ofstream::out);
		
			outfile << 
			"1-voxGrid :" << voxGrid <<"\n" <<
			"2-voxAplication :" << vox <<"\n" <<
			"3-transforEpsilon :" << transforEpsilon <<"\n" <<
			"4-MaxIterationsAlig :" << MaxIterationsAlig <<"\n" <<
			"5-rMaxCorrespondenceDistance :" << rMaxCorrespondenceDistance <<"\n" <<
			"6-iterationsFor :" << IterationsFor <<"\n" <<
			"7-normalKserach :" << normalKserach <<"\n" <<
			"8-MaxCorrDisSubs :" << MaxCorrDisSubs <<"\n" <<
			"9-Jumps :" << jumps <<"\n" <<
			"10-FloorRemoval :" << Bfloor <<"\n" << std::endl;
			outfile.close();

	  	}

	  	void loadFolder(){
	  		stringstream ss;
				snapCount=0;

				std::cout << "\n*************************\nEnter the name of the folder to rebuild\nIt must be on the same path as the executable" << std::endl;
				std::cin >> pathToLoad;
				std::cout << "\n*************************\nEnter the name of the files without number\n Example:\n captura --> This will rebuild files captur0.pcd" << std::endl;
				std::cin >> name;
				url = pathToLoad+"/"+name+"0.pcd";

				std::cout << "\n*************************\nNumber of jumps in reconstruction(Default use 0)" << std::endl;
				std::cin >> jumps;
				ss << jumps;

				std::cout <<"\nPath to Load is : "+url + "\nJump between files: "+ss.str()<< std::endl;	
				//string command="find "+ pathToLoad +" -type f -iname \"*.pcd\" | wc -l";
				//numFiles=system((command).c_str());

				std::cout <<"Last File Number?\n";
				std::cin >> numFiles;
				string command="mkdir "+pathToLoad+"/"+pathReconstruct;
				system((command).c_str());
				std::cout <<std::endl;				
	  	}
	  	void configure(){
	  		int tipe=10;
				while(tipe!=0){
					std::cout << "\nConfigure choose number:\n"<<
					"1-voxGrid :" << voxGrid <<"\n" <<
					"2-voxAplication :" << vox <<"\n" <<
					"3-transforEpsilon :" << transforEpsilon <<"\n" <<
					"4-MaxIterationsAlig :" << MaxIterationsAlig <<"\n" <<
					"5-rMaxCorrespondenceDistance :" << rMaxCorrespondenceDistance <<"\n" <<
					"6-iterationsFor :" << IterationsFor <<"\n" <<
					"7-normalKserach :" << normalKserach <<"\n" <<
					"8-MaxCorrDisSubs :" << MaxCorrDisSubs <<"\n" <<
					"9-Jumps :" << jumps <<"\n" <<
					"10-FloorRemoval :" << Bfloor <<"\n" <<
					"0-Exit\n" << std::endl;
					
					std::cin >> tipe;
					std::cout << "Introduce new value:\n";
					switch(tipe){
						case 1: std::cin >> voxGrid ;
								break;
						case 2: std::cin >> vox ;
								break;
						case 3: std::cin >> transforEpsilon ;
								break;
						case 4: std::cin >> MaxIterationsAlig ;
								break;
						case 5: std::cin >> rMaxCorrespondenceDistance ;
								break;
						case 6: std::cin >> IterationsFor ;
								break;
						case 7: std::cin >> normalKserach ;
								break;
						case 8: std::cin >> MaxCorrDisSubs ;
								break;
						case 9: std::cin >> jumps ;
								break;
						case 10: std::cin >> Bfloor ;
								break;
						default: 
								break;
					}

				}
	  	}
	  	void savePCD(const pcl::PointCloud<PointT>::ConstPtr &cloud){
			stringstream ss;
			ss<< snapCount;

			string saveTo = pathToLoad+"/"+pathReconstruct+"/"+ss.str()+".pcd";
			pcl::io::savePCDFile (saveTo.c_str(), *cloud,true);  //guardamos la nube de puntos en .pcd
		}
		void saveRecons(const pcl::PointCloud<PointT>::ConstPtr &cloud){
			stringstream ss;
			ss<< snapCount;

			string saveTo = pathToLoad+"/"+pathReconstruct+"/Reconstruction"+ss.str()+".pcd";
			pcl::io::savePCDFile (saveTo.c_str(), *cloud,true);  //guardamos la nube de puntos en .pcd
		}
		int menu(){
			int menu=10;
			int maxfile=0;
				while(menu!=0){
					std::cout<< "************\nInitializing Reconstruction Menu\n"<<
								"Default Folder and file names:\n"<<
								"snaps/snap_point0.pcd\n"<<
								"\n 1 to to load folder and files\n"<<
								" 2 to configure\n"<<
								" 3 to Save configure\n"<<
								" 4 to next Image Union\n"<<
								" 5 reconstruct to number\n"<<
								" 6 to reconstruct to final\n"<<
								" 7 Load reconstruction Directory\n"<<
								" 8 Load PCD\n"<<
								" 9 Erase viewer and star from 0 \n"<<
								" 10 number to number \n"<<
								" 0 Exit program \n"<<
								" Close Reconstruction viewer to Exit\n"<<std::endl;
					
					std::cin >> menu;

					switch(menu){
						case 1: loadFolder();
								break;

						case 2: configure() ;
								break;

						case 3: saveConfig();
								break;

						case 4: unir(snapCount+1+jumps);
								break;

						case 5: 
								std::cout << "Number to reconstruct:\n";
								
								std::cin >> maxfile;

								unir(maxfile);
								break;

						case 6: 
								saveConfig();
								unir(numFiles);
								break;

						case 7: loadDirectory();
								break;

						case 8: viewerUnion->removeAllPointClouds();
								viewerReconstruction->removeAllPointClouds();
								loadPCD();
								break;

						case 9: snapCount=0;
								viewerUnion->removeAllPointClouds();
								viewerReconstruction->removeAllPointClouds();
								saveConfig();
								
								std::cout << "Number to reconstruct:\n";
								
								std::cin >> maxfile;

								unir(maxfile);
								break;

						case 10: viewerUnion->removeAllPointClouds();
								viewerReconstruction->removeAllPointClouds();
								snapCount=0;
								std::cout << "Number to reconstruct from:\n";
								std::cin >> snapCount;
								std::cout << "Number to reconstruct:\n";
								std::cin >> maxfile;

								unir(maxfile);
								break;
						default:
								break;
					}

				}
			
				return menu;
		}

		////////////////////////////////////////////////////////////////////////////////
		/** \brief Display source and target on the first viewport of the visualizer
		*
		*/
		void showCloudsLeft(const PointCloud::Ptr cloud_target, const PointCloud::Ptr cloud_source){
			viewerUnion->removePointCloud ("vp1_target");
			viewerUnion->removePointCloud ("vp1_source");

			PointCloudColorHandlerCustom<PointT> tgt_h (cloud_target, 0, 255, 0);
			PointCloudColorHandlerCustom<PointT> src_h (cloud_source, 255, 0, 0);
			viewerUnion->addPointCloud (cloud_target, tgt_h, "vp1_target", vp_1);
			viewerUnion->addPointCloud (cloud_source, src_h, "vp1_source", vp_1);
			viewerUnion->spinOnce();
		}


		////////////////////////////////////////////////////////////////////////////////
		/** \brief Display source and target on the second viewport of the visualizer
		*
		*/
		void showCloudsRight(const PointCloudWithNormals::Ptr cloud_target, const PointCloudWithNormals::Ptr cloud_source){
			viewerUnion->removePointCloud ("source");
			viewerUnion->removePointCloud ("target");


			PointCloudColorHandlerGenericField<PointNormalT> tgt_color_handler (cloud_target, "curvature");
			if (!tgt_color_handler.isCapable ())
			PCL_WARN ("Cannot create curvature color handler!");

			PointCloudColorHandlerGenericField<PointNormalT> src_color_handler (cloud_source, "curvature");
			if (!src_color_handler.isCapable ())
			PCL_WARN ("Cannot create curvature color handler!");


			viewerUnion->addPointCloud (cloud_target, tgt_color_handler, "target", vp_2);
			viewerUnion->addPointCloud (cloud_source, src_color_handler, "source", vp_2);

			viewerUnion->spinOnce();
			viewerReconstruction->spinOnce();
		}

		void loadDirectory(){

			viewerReconstruction->removeAllPointClouds();
			
			stringstream ss;
			string aux;
			std::cout << "\n*************************\nEnter the path" << std::endl;
			std::cin >> aux;
			bool vox;
			float leaf=0.01f;
			std::cout << "\n*************************\nVoxGrid? -1 yes -0 no" << std::endl;
			std::cin >> vox;
			std::cout << "\n*************************\nGrid size? -0.01f default" << std::endl;
			std::cin >> leaf;
			

			std::cout <<"Number of files?\n";
			std::cin >> numFiles;
		
			std::cout <<"archivos: "<<numFiles<<std::endl;
				
				reconstruction.reset(new pcl::PointCloud<PointT> ()); 		

				pcl::PointCloud<PointT>::Ptr load1 (new pcl::PointCloud<PointT> ());
				PointCloud::Ptr toAdd (new PointCloud);
				string url;
				pcl::console::print_highlight ("Loading Files...\n");
				for(snapCount=1;snapCount<=numFiles;snapCount++){
					ss.str("");
					ss<<snapCount;
					url=aux+ss.str()+".pcd";					
					std::cout <<url<<std::endl;
					if (pcl::io::loadPCDFile<PointT> (url, *load1) == -1){ //* load the file
						PCL_ERROR ("Couldn't read file\n");
					}else{
						if(vox){
							pcl::VoxelGrid<PointT> grid;
							grid.setLeafSize (leaf,leaf,leaf);
							grid.setInputCloud (load1);
							grid.filter (*toAdd);
						}
						else{
							*toAdd=*load1;
						}
						*reconstruction += *toAdd;

						viewerReconstruction->removeAllPointClouds();
						addCloud(reconstruction);
					}
				}
				cout<<"Reconstruction Finalice, press q into viewer to return to menu\n";
				viewerReconstruction->spin();
				snapCount=0;	
		}

		void loadPCD(){
			
			PointCloud::Ptr pcd (new PointCloud);
			stringstream ss;
			string url;

				snapCount=0;
				std::cout << "\n*************************\nEnter the file" << std::endl;
				std::cin >> url;
				if (pcl::io::loadPCDFile<PointT> (url, *pcd) == -1){ //* load the file
					PCL_ERROR ("Couldn't read file\n");
				}else{

					addCloud(pcd);
					cout<<"Reconstruction Finalice, press q into viewer to return to menu\n";
				viewerReconstruction->spin();
				}
		}

	  	void addCloud(const pcl::PointCloud<PointT>::ConstPtr &cloud){
	  		std::ostringstream oss;
			oss << "model_"<< snapCount;		
			viewerReconstruction->addPointCloud(cloud,oss.str());
			viewerReconstruction->spinOnce();
	  	}

	  	void removeNan( PointCloud::Ptr &cloud){
	  		std::vector<int> indices;
      		pcl::removeNaNFromPointCloud(*cloud, *cloud, indices);
	  	}

	  	void removeFloor(pcl::PointCloud<PointT>::Ptr &cloud){

	  		pcl::PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>), cloud_p (new pcl::PointCloud<PointT>), cloud_f (new pcl::PointCloud<PointT>);
			pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
			pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
			// Create the segmentation object
			pcl::SACSegmentation<PointT> seg;
			// Optional
			seg.setOptimizeCoefficients (true);
			// Mandatory
			seg.setModelType (pcl::SACMODEL_PLANE);
			seg.setMethodType (pcl::SAC_RANSAC);
			seg.setMaxIterations (1000);
			seg.setDistanceThreshold (0.02);

			// Create the filtering object
			pcl::ExtractIndices<PointT> extract;

				// Segment the largest planar component from the remaining cloud
				seg.setInputCloud (cloud);
				seg.segment (*inliers, *coefficients);

				// Extract the inliers
				extract.setInputCloud (cloud);
				extract.setIndices (inliers);
				extract.setNegative (false);
				extract.filter (*cloud_p);
				
				// Create the filtering object
				extract.setNegative (true);
				extract.filter (*cloud_f); 
				cloud.swap (cloud_f);
	  	}

	  	void unir(int numero){
			
			while(numero>=snapCount+1+jumps){

					pcl::console::print_highlight ("Loading Files...\n");
					stringstream ss;
					{	pcl::ScopeTime t(" \t\t\t");

						if(error!=0){
							int i;
							if(jumps==0){
								i=snapCount - error;
							}
							else{
								i=snapCount - jumps*error;
							}

							ss << i;
							error=false;
						}
						else{
							ss << snapCount;
						}
						url =pathToLoad+"/"+name+ss.str()+".pcd";
						std::cout<<url;
						std::cout<<std::endl;
						if (pcl::io::loadPCDFile (url, *source) == -1){ //* load the file
							PCL_ERROR ("Couldn't read file\n");
							goto ERROR;
						}else{
							if(Bfloor){
								removeFloor(source);
							}
							removeNan(source);
						}
						ss.str("");	
						snapCount=snapCount+1+jumps;
						ss << snapCount;

						url =pathToLoad+"/"+name+ss.str()+".pcd";
						std::cout<<url;
						std::cout<<std::endl;
						if (pcl::io::loadPCDFile (url, *target) == -1){ //* load the file
							PCL_ERROR ("Couldn't read file\n");
							goto ERROR;
						}
						else{
							if(Bfloor){
								removeFloor(target);
							}
							removeNan(target);
						}	
					}
					// Add visualization data
					showCloudsLeft(source, target);

					PointCloud::Ptr temp (new PointCloud);
					pairAlign (source, target, temp, pairTransform, vox);

					//transform current pair into the global transform
					pcl::transformPointCloud (*temp, *result, GlobalTransform);

					//update the global transform
					GlobalTransform = GlobalTransform * pairTransform;

					*reconstruction+=*result;
					saveRecons(result);
					PointCloud::Ptr reconsViewer (new PointCloud);

					pcl::VoxelGrid<PointT> grid;
					float leaf=0.005f;
					grid.setLeafSize (leaf,leaf,leaf);
					grid.setInputCloud (result);
					grid.filter (*reconsViewer);

					addCloud(result);

				}
			//}
			cout<<"Reconstruction Finalice.\n";
			if(false){
				ERROR: cout<<"error loading files"<<std::endl;
			}
			cout <<"Press q into viewer to return to menu\n";	
			viewerReconstruction->spin();
			
		}

		////////////////////////////////////////////////////////////////////////////////
		/** \brief Align a pair of PointCloud datasets and return the result
		* \param cloud_src the source PointCloud
		* \param cloud_tgt the target PointCloud
		* \param output the resultant aligned source PointCloud
		* \param final_transform the resultant transform between source and target
		*/
		void pairAlign (const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, PointCloud::Ptr output, Eigen::Matrix4f &final_transform, bool downsample)
		{
			//
			// Downsample for consistency and speed
			// \note enable this for large datasets
			PointCloud::Ptr src (new PointCloud);
			PointCloud::Ptr tgt (new PointCloud);
			pcl::VoxelGrid<PointT> grid;

			if (downsample)
			{
				grid.setLeafSize (voxGrid,voxGrid,voxGrid);
				grid.setInputCloud (cloud_src);
				grid.filter (*src);

				grid.setInputCloud (cloud_tgt);
				grid.filter (*tgt);
			}
			else
			{
				src = cloud_src;
				tgt = cloud_tgt;
			}
			
				// Compute surface normals and curvature
				PointCloudWithNormals::Ptr points_with_normals_src (new PointCloudWithNormals);
				PointCloudWithNormals::Ptr points_with_normals_tgt (new PointCloudWithNormals);

			pcl::console::print_highlight ("Calculating Normals...");
			{	pcl::ScopeTime t(" \t\t\t");

				pcl::NormalEstimationOMP<PointT, PointNormalT> norm_est;
				norm_est.setKSearch (normalKserach);

				norm_est.setInputCloud (src);
				norm_est.compute (*points_with_normals_src);
				pcl::copyPointCloud (*src, *points_with_normals_src);

				norm_est.setInputCloud (tgt);
				norm_est.compute (*points_with_normals_tgt);
				pcl::copyPointCloud (*tgt, *points_with_normals_tgt);
			}


			//
			// Instantiate our custom point representation (defined above) ...
			MyPointRepresentation point_representation;
			// ... and weight the 'curvature' dimension so that it is balanced against x, y, and z
			float alpha[4] = {1.0, 1.0, 1.0, 1.0};
			point_representation.setRescaleValues (alpha);

			

				//
				// Align
				pcl::IterativeClosestPointNonLinear<PointNormalT, PointNormalT> reg;

			pcl::console::print_highlight ("Starting alignment...\n");
			{	pcl::ScopeTime t(" \t\t\t");

				reg.setTransformationEpsilon (transforEpsilon);
				// Set the maximum distance between two correspondences (src<->tgt) to 10cm
				// Note: adjust this based on the size of your datasets
				reg.setMaxCorrespondenceDistance (rMaxCorrespondenceDistance);  
				// Set the point representation
				reg.setPointRepresentation (boost::make_shared<const MyPointRepresentation> (point_representation));

				reg.setInputSource (points_with_normals_src);
				reg.setInputTarget (points_with_normals_tgt);


				//
				// Run the same optimization in a loop and visualize the results
				Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), prev, targetToSource;
				PointCloudWithNormals::Ptr reg_result = points_with_normals_src;
				reg.setMaximumIterations (MaxIterationsAlig);
				pcl::console::print_highlight ("Starting for...\n");
				for (int i = 0; i < IterationsFor; ++i)
				{
					//PCL_INFO ("Iteration Nr. %d.\n", i);

					// save cloud for visualization purpose
					points_with_normals_src = reg_result;

					// Estimate
					reg.setInputSource (points_with_normals_src);
					reg.align (*reg_result);
					//accumulate transformation between each Iteration
					Ti = reg.getFinalTransformation () * Ti;

					//if the difference between this transformation and the previous one
					//is smaller than the threshold, refine the process by reducing
					//the maximal correspondence distance
					if (fabs ((reg.getLastIncrementalTransformation () - prev).sum ()) < reg.getTransformationEpsilon ())
					reg.setMaxCorrespondenceDistance (reg.getMaxCorrespondenceDistance () - MaxCorrDisSubs);

					prev = reg.getLastIncrementalTransformation ();
					// visualize current state
					showCloudsRight(points_with_normals_tgt, points_with_normals_src);
				}

				//
				// Get the transformation from target to source
				targetToSource = Ti.inverse();

				//
				// Transform target back in source frame
				pcl::transformPointCloud (*cloud_tgt, *output, targetToSource);

				viewerUnion->removePointCloud ("source");
				viewerUnion->removePointCloud ("target");

				PointCloudColorHandlerCustom<PointT> cloud_tgt_h (output, 0, 255, 255);
				PointCloudColorHandlerCustom<PointT> cloud_src_h (cloud_src, 255, 255, 0);
				viewerUnion->addPointCloud (output, cloud_tgt_h, "target", vp_2);
				viewerUnion->addPointCloud (cloud_src, cloud_src_h, "source", vp_2);

				//add the source to the transformed target
				*output += *cloud_src;

				final_transform = targetToSource;
			}
		}


	  	void init(){
	  		//inicialice 

	  		PointCloud::Ptr source (new PointCloud);
	  		PointCloud::Ptr target (new PointCloud);
	  		PointCloud::Ptr result (new PointCloud);
	  		PointCloud::Ptr reconstruction (new PointCloud);
	  		this->result=result;
	  		this->source=source;
	  		this->target=target;
	  		this->reconstruction=reconstruction;
		
	  		Bfloor=true;
	  		vox=true;
	  		MaxCorrDisSubs=0.001;
	  		error=0;
			voxGrid=0.01; 
			transforEpsilon=0.0000001;
			MaxIterationsAlig=100000;
			rMaxCorrespondenceDistance=0.05; //0.05 maxima distancia correspondencia 5 cm  .09
			normalKserach=30; 
			IterationsFor=50;
			pathToLoad = "snaps";
			name="snap_point";
			pathReconstruct ="reconstruction";
			string command="mkdir "+pathToLoad+"/"+pathReconstruct;
			system((command).c_str());
			GlobalTransform= Eigen::Matrix4f::Identity ();

			//default folder and name
			snapCount = 0;
			jumps=0;
	  	}
	
	public:	
		void run(){

			init();
			//inicilice viewer and parameters of viewer
			boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer2 (new pcl::visualization::PCLVisualizer ("Union"));
			boost::shared_ptr<pcl::visualization::PCLVisualizer>  one (new pcl::visualization::PCLVisualizer ("Reconstruction"));	
			this->viewerReconstruction = one;
			this->viewerUnion = viewer2;
			viewerUnion->initCameraParameters ();
			viewerReconstruction->initCameraParameters ();
			viewerUnion->setBackgroundColor (0, 0, 0);
			viewerReconstruction->setBackgroundColor (0, 0, 0);
			viewerReconstruction->setCameraPosition(0,0,-2,0,-1,0,0);
			viewerUnion->setCameraPosition(0,0,-2,0,-1,0,0);
			viewerUnion->createViewPort (0.0, 0, 0.5, 1.0, vp_1);
			viewerUnion->createViewPort (0.5, 0, 1.0, 1.0, vp_2);
			
			while (menu()!=0)
			{
				boost::this_thread::sleep (boost::posix_time::microseconds (100000));
			}
		}
	
};
