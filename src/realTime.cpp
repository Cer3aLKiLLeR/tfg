#include <iostream>
#include <exception>
#include <string.h>
#include <libusb-1.0/libusb.h>  				// kinect
#include <pcl/visualization/pcl_visualizer.h> 	// visualizador pcl
#include <pcl/point_cloud.h> 					//	
#include <pcl/io/pcd_io.h> 						// save pcd
#include <pcl/io/openni_grabber.h>				// grabber
#include <pcl/io/png_io.h>						// sabe png
#include <stdlib.h>
#include <fstream>

#include <pcl/filters/filter.h>					// remove nan
#include <pcl/point_types.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/board.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/point_cloud_color_handlers.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/common/time.h>

//planes
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>

#include <boost/make_shared.hpp>
#include <pcl/point_representation.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>

using namespace std;

class RealTime{

	private:
		boost::shared_ptr<pcl::visualization::PCLVisualizer> 	viewerRT;
	  	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr 				cloudViewRT;

	  	string pathToSave;
	  	int snapCount;

		void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,void* viewerVoid){

			if (event.getKeySym () == "s" && event.keyDown ()){

				stringstream ss;
				ss << snapCount;
				string saveTo = pathToSave+"snap_";					//añadimos ruta y nombre con numero al archivo
				std::cout << "Saving..." << std::endl;
				pcl::io::savePCDFileASCII (saveTo+"point"+ss.str()+".pcd", *cloudViewRT);  //guardamos la nube de puntos en .pcd
				pcl::io::savePNGFile(saveTo+"color"+ss.str()+".png", *cloudViewRT);
				snapCount++;													//cuenteo++
			}
		}
		
		void cloud_cb_ (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr &cloud){

			pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloudInput(new pcl::PointCloud<pcl::PointXYZRGBA>);
			pcl::copyPointCloud(*cloud, *cloudInput);

			cloudViewRT = cloudInput;

			viewerRT->removeAllPointClouds();
			pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBA> rgb(cloudViewRT);
			viewerRT->addPointCloud<pcl::PointXYZRGBA> (cloudViewRT, rgb, "cloudRT");
			viewerRT->spinOnce();
			
		}

	public:
		void run(){	
			std::cout<< "*************************\nInitializing Broadcast\nPress S to save file\n "<<std::endl;

			system("mkdir snaps");
			pathToSave = "snaps/";
			snapCount = 0;

			// try{
				// create a new grabber for OpenNI devices
			pcl::Grabber* interface = new pcl::OpenNIGrabber();

			// Creamos el viewer y lo metemos en su propio hilo
			boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("RealTime"));
			viewer->registerKeyboardCallback(&RealTime::keyboardEventOccurred,*this); 
			viewer->setBackgroundColor (0, 0, 0);
			viewer->initCameraParameters ();
			viewer->setCameraPosition(0,0,-2,0,-1,0,0);
			this->viewerRT = viewer; 

		
			//fill grabber with signal
			boost::function<void (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&)> f = boost::bind (&RealTime::cloud_cb_, this, _1);
			boost::signals2::connection c = interface->registerCallback (f);
						
			interface->start ();

			while (!viewerRT->wasStopped()){
				boost::this_thread::sleep (boost::posix_time::microseconds (100000));
			}
			interface->stop ();			
		}	
};