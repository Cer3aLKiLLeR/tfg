cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(MYPROJECT)

find_package(PCL 1.7 REQUIRED)
FIND_PACKAGE(Boost COMPONENTS program_options REQUIRED)
find_package(VTK REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})
file (GLOB SOURCES *.cpp)

add_executable (proyecto ${SOURCES})
target_link_libraries (proyecto ${PCL_LIBRARIES} ${OpenCV_LIBS} ${Boost_LIBRARIES} ${VTK_LIBRARIES})

