#include "reconstruction.cpp"

using namespace std;

int main(int argc, char *argv[]){

	printf("\n*************************\nAvailable options:\n-c\tTo inicialice BroadCast\n-f\tTo inicialice Reconstruction\n\n");
	bool point,reconsNoViewer;
	point=false;reconsNoViewer=false;

	while (*++argv){
    if ((*argv)[0] == '-'){
        switch ((*argv)[1]) { 
        
            default:
                printf("Unknown option -%c\n\n", (*argv)[1]);
                break;
            case 'c':
            	point = true;
                break;
            case 'f':
            	reconsNoViewer = true;
                break;
        }
    }
	}
	Reconstruction 	v;
	RealTime 		r;
	boost::thread* thr1;
	if(!reconsNoViewer){

		if(point)
			thr1 = new boost::thread(&RealTime::run, r);
		
		if(point)
			thr1->join();
	}else{
		v.run();
	}

	
	return (0);
}
